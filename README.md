
<!-- README.md is generated from README.Rmd. Please edit that file -->

# satRday Johannesbourg workshop

Building Successful Shiny Apps with `{golem}`

## <https://gitlab.com/colin_fay/golem-joburg>

## Schedule

  - 9:00 10:30 - Part 00 & 01: Introduction

  - 10:30 11:00 - Break ☕ / 🍵

  - 11:00 12:30 - Part 02: Developing a golem app

  - 12:30 13:30 - Lunch 🍱

  - 13:30 15:00 - JavaScript & CSS

  - 13:30 15:00 - Break ☕ / 🍵

  - 15:30 17:00 - Testing & Sending to prod

### About each session

Each session is split in two:

  - 40 to 45 minutes of presentation

  - 45 to 50 minutes of practice

## `{golem}` version

You need version 0.2.1 for this workshop.

### Check with:

``` r
packageVersion("golem")
```

### If not

``` r
install.package("golem")
```

### If it’s not yet on the CRAN near you:

Download the tar gz [golem.tar.gz](golem.tar.gz)

``` r
remotes::install_local("path/to/golem.tar.gz")
```

## Logistics

  - 🔗 <https://github.com/ColinFay/golem-joburg>

  - 📡 PwC-guest: gdawson1 / D#w3H

  - 🐤 [@\_ColinFay](https://twitter.com/_ColinFay)

  - 🐤 [@satRday\_ZAF](https://twitter.com/satRday_ZAF)

  - 💪 <https://joburg2020.satrdays.org/code-of-conduct-2020.html>
